package com.example.samplesensor.activity

import android.Manifest
import android.os.Bundle
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.content.Intent
import android.widget.Switch
import android.hardware.SensorManager
import android.hardware.Sensor
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.samplesensor.Constants
import com.example.samplesensor.settings.AppSettings
import com.example.samplesensor.utils.Utils
import com.example.samplesensor.view.LineGraphView
import com.example.samplesensor.dialog.SettingsDialog
import com.example.samplesensor.service.StabilizationService
import com.otaliastudios.cameraview.CameraException
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraView
import kotlinx.android.synthetic.main.activity_main.*
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.samplesensor.utils.FileWriterUtils
import java.io.*


class MainActivity : AppCompatActivity() {

    private lateinit var layoutSensor: View

    private lateinit var graph1: LineGraphView
    private lateinit var graph2: LineGraphView
    private lateinit var graph3: LineGraphView

    private lateinit var viewFinder: CameraView


    private lateinit var sensorManager: SensorManager
    private lateinit var accelerometer: Sensor

    private val tempAcc = FloatArray(3)
    private val acc = FloatArray(3)
    private val velocity = FloatArray(3)
    private val position = FloatArray(3)
    private var timestamp: Long = 0
    private val videoAbsolutePath = "/storage/emulated/0/videoAnalytics.mp4"

    private val fileAbsolutePath = "/storage/emulated/0/dataInfo.txt"

    private var file:File? = null

    private var fileWriter: BufferedWriter? = null

    private var settings: AppSettings? = null


    private var prevTime = 0L

    private var currTime = 0L

    private val sensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}

        override fun onSensorChanged(event: SensorEvent) {
            if (timestamp != 0L) {
                tempAcc[0] =
                    Utils.rangeValue(event.values[0], -Constants.MAX_ACC, Constants.MAX_ACC)
                tempAcc[1] =
                    Utils.rangeValue(event.values[1], -Constants.MAX_ACC, Constants.MAX_ACC)
                tempAcc[2] =
                    Utils.rangeValue(event.values[2], -Constants.MAX_ACC, Constants.MAX_ACC)

                settings?.lowPassAlpha?.let { Utils.lowPassFilter(tempAcc, acc, it) }

                val dt = (event.timestamp - timestamp) * Constants.NS2S

                for (index in 0..2) {
                    settings?.let {
                        velocity[index] += acc[index] * dt - it.velocityFriction * velocity[index]
                    }
                    velocity[index] = Utils.fixNanOrInfinite(velocity[index])

                    settings?.let {
                        position[index] += velocity[index] * it.velocityAmpl * dt - it.positionFriction * position[index]
                    }
                    position[index] = Utils.rangeValue(
                        position[index],
                        -Constants.MAX_POS_SHIFT,
                        Constants.MAX_POS_SHIFT
                    )
                }
            } else {
                velocity[2] = 0f
                velocity[1] = velocity[2]
                velocity[0] = velocity[1]
                position[2] = 0f
                position[1] = position[2]
                position[0] = position[1]

                acc[0] = Utils.rangeValue(event.values[0], -Constants.MAX_ACC, Constants.MAX_ACC)
                acc[1] = Utils.rangeValue(event.values[1], -Constants.MAX_ACC, Constants.MAX_ACC)
                acc[2] = Utils.rangeValue(event.values[2], -Constants.MAX_ACC, Constants.MAX_ACC)
            }
            timestamp = event.timestamp
            currTime = System.currentTimeMillis()
//            val millis = System.currentTimeMillis() + ((timestamp - System.nanoTime()) / 1000000L)
            recordFile(currTime - prevTime,acc, position, velocity)
            layoutSensor.translationX = -position[0]
            layoutSensor.translationY = position[1]

            graph1.setValue(acc[0])
            graph2.setValue(acc[1])
            graph3.setValue(acc[2])
        }
    }

    private val cameraListener = object : CameraListener() {
        override fun onVideoRecordingStart() {
            super.onVideoRecordingStart()
            prevTime = System.currentTimeMillis()
            val ret = sensorManager.registerListener(
                sensorEventListener,
                accelerometer,
                SensorManager.SENSOR_DELAY_FASTEST
            )
            if (!ret) {
                Log.e(TAG, "Sensor listener registration failed")
            }
        }

        override fun onVideoRecordingEnd() {
            super.onVideoRecordingEnd()
            saveDataToFile(fileWriter,"", true)
            fileWriter = null
            sensorManager.unregisterListener(sensorEventListener)


        }

        override fun onCameraError(exception: CameraException) {
            super.onCameraError(exception)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value", "Permission Granted, Now you can use local drive .")
            } else {
                Log.e("value", "Permission Denied, You cannot use local drive .")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.samplesensor.R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    PERMISSION_REQUEST_CODE)
            }
        }
//        setSvcEnabled(true)
        viewFinder = findViewById(com.example.samplesensor.R.id.view_finder)

        viewFinder.setLifecycleOwner(this)
        viewFinder.addCameraListener(cameraListener)
        settings = AppSettings.getAppSettings(applicationContext)

        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)

        initViews()
        reset()

        val switchSvc = findViewById<Switch>(com.example.samplesensor.R.id.switch_svc)
        switchSvc?.isChecked = settings?.isServiceEnabled ?: false

        switchSvc?.setOnCheckedChangeListener { _, isChecked -> setSvcEnabled(isChecked) }

        startRecord.setOnClickListener{
            file = File(fileAbsolutePath)
            fileWriter = BufferedWriter(FileWriter(file))
            var header = "|time    |Accel               |Posit        |Veloc        |\n"
            saveDataToFile(fileWriter,header, false)
            header =         "|time    |   x  |   y  |   z  |   x  |   y  |   x  |   y  |\n"
            saveDataToFile(fileWriter,header, false)
            viewFinder.takeVideo(File(videoAbsolutePath))
            reset()
            recordFile(timestamp, acc, position, velocity)
        }

        stopRecord.setOnClickListener{
            viewFinder.stopVideo()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    private fun initViews() {
        layout_root.setOnClickListener { reset() }

        layoutSensor = findViewById(com.example.samplesensor.R.id.layout_sensor)
        graph1 = findViewById(com.example.samplesensor.R.id.graph1)
        graph2 = findViewById(com.example.samplesensor.R.id.graph2)
        graph3 = findViewById(com.example.samplesensor.R.id.graph3)

        graph1.setOnClickListener {graph1.clear()}

        graph2.setOnClickListener{ graph2.clear() }

        graph3.setOnClickListener{graph3.clear()}
    }

    private fun showSettings() {
        SettingsDialog.show(this)
    }

    private fun setSvcEnabled(enabled: Boolean) {
        if (enabled)
            startService(Intent(this, StabilizationService::class.java))
        else
            stopService(Intent(this, StabilizationService::class.java))

        settings?.isServiceEnabled = enabled
        settings?.saveDeferred()
    }

    private fun reset() {
        position[2] = 0f
        position[1] = position[2]
        position[0] = position[1]
        velocity[2] = 0f
        velocity[1] = velocity[2]
        velocity[0] = velocity[1]
        acc[2] = 0f
        acc[1] = acc[2]
        acc[0] = acc[1]
        timestamp = 0

        layoutSensor.translationX = 0F
        layoutSensor.translationY = 0F
    }

    private fun recordFile(timestamp: Long, acc: FloatArray, position: FloatArray, velocity: FloatArray) {
//        val millis = timestamp / 1000000L
        val time = getFormatTime(timestamp)
        val acc0 = String.format(format, acc[0])
        val acc1 = String.format(format, acc[1])
        val acc2 = String.format(format, acc[2])
        val position0 = String.format(format, position[0])
        val position1 = String.format(format, position[1])
        val velocity0 = String.format(format, velocity[0])
        val velocity1 = String.format(format, velocity[1])
        val text = "$time|$acc0|$acc1|$acc2|$position0|$position1|$velocity0|$velocity1\n"
        saveDataToFile(fileWriter, text, false)
    }

    private fun saveDataToFile(fileWriter: BufferedWriter?, text: String, endFile: Boolean) {
        try {
            fileWriter?.write(text)
        } catch (ex: IOException) {
            ex.printStackTrace()
            Log.e(TAG, "ERROR FILE")
        } finally {
            if (endFile) {
                fileWriter?.close()
            }
        }
    }

    private fun getFormatTime(timestamp: Long): String {
        val min = (timestamp / 60000) % 60
        val sec = (timestamp / 1000) % 60
        val ms1 = timestamp - (min * 60000) - (sec * 1000)
        return String.format("%02d:%02d:%03d", min, sec, ms1)
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val PERMISSION_REQUEST_CODE = 100
        const val format = "%+06.3f"
    }
}
