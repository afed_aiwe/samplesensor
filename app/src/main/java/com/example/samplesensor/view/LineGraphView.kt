package com.example.samplesensor.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.View


class LineGraphView : View {

    private val handle = Handler()

    private val lp = Paint()

    private lateinit var data: FloatArray
    private var framePos = 0
    private var lineWidth = 0
    private var maxAbsValue = 0f
    private var currValue = 0f

    private val backgroundColor = -0xdfdfe0
    private val borderColor = -0x1
    private val axisColor = -0x8f7f70
    private val dataColor = -0x7f70

    private var isAttached = false

    private var stringHeight: Int = 0
    private var stringWidth: Int = 0

    private val updateRunnable = object : Runnable {
        override fun run() {
            Log.e(TAG, "run")
            // shift data
            framePos++
            Log.e(TAG, "run framepos = $framePos and lineWidth-1 = ${lineWidth-1}")
            if (framePos == lineWidth - 1) {
                framePos = 0
                Log.e(TAG, "run shift data")
                // shift data
                var i = 0
                data.forEach {
                    Log.e(TAG, "run data[$i] = $it")
                    i++
                }
                Log.e(TAG, "run srcPos = ${(lineWidth - 1) * 4} and length = ${data.size / 2}")
                System.arraycopy(data, (lineWidth - 1) * 4, data, 0, data.size / 2)
                i = 0
                data.forEach {
                    Log.e(TAG, "run data[$i] = $it")
                    i++
                }
                // fill X
                fillX()
            } else {
                data[(framePos + lineWidth - 1) * 4 + 1] = data[(framePos - 1 + lineWidth - 1) * 4 + 3]
                Log.e(TAG, "run framePos data[${(framePos + lineWidth - 1) * 4 + 1}]=${data[(framePos - 1 + lineWidth - 1) * 4 + 3]}")
                data[(framePos + lineWidth - 1) * 4 + 3] = currValue
                Log.e(TAG, "run framePos data[${(framePos + lineWidth - 1) * 4 + 3}]=$currValue")
            }

            invalidate()
            if (isAttached) handle.postDelayed(this, UPDATE_DELAY.toLong())
        }
    }

    constructor(context: Context) : super(context) {
        Log.e(TAG,"constructor1")
        lp.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            TEXT_SIZE.toFloat(),
            resources.displayMetrics
        )
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        Log.e(TAG,"constructor2")
        lp.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            TEXT_SIZE.toFloat(),
            resources.displayMetrics
        )
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        Log.e(TAG,"onSizeChanged")
        lineWidth = w
        framePos = 0
        Log.e(TAG, "float size = ${(w * 2 - 1) * 4}")
        data = FloatArray((w * 2 - 1) * 4)
        clearData()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        Log.e(TAG,"onAttachedToWindow")
        isAttached = true
        handle.postDelayed(updateRunnable, UPDATE_DELAY.toLong())
    }

    override fun onDetachedFromWindow() {
        Log.e(TAG,"onDetachedFromWindow")
        isAttached = false
        handle.removeCallbacks(updateRunnable)
        super.onDetachedFromWindow()
    }

    private fun clearData() {
        Log.e(TAG,"clearData")
        Log.e(TAG, "data.size / 4 = ${data.size / 4}")
        for (x in 0 until data.size / 4) {
            data[x * 4] = x.toFloat()
            Log.e(TAG, "data[x * 4] = ${data[x * 4]}")
            data[x * 4 + 1] = 0f
            Log.e(TAG, "data[${x * 4 + 1}] = ${data[x * 4 + 1]}")
            data[x * 4 + 2] = (x + 1).toFloat()
            Log.e(TAG, "data[${x * 4 + 2}] = ${data[x * 4 + 2]}")
            data[x * 4 + 3] = 0f
            Log.e(TAG, "data[${x * 4 + 3}] = ${data[x * 4 + 3]}")
        }
    }

    fun setValue(value: Float) {
        Log.e(TAG,"setValue value = $value")
        currValue = value
        setAbsValue(value)
    }

    private fun setAbsValue(value: Float) {
        Log.e(TAG,"setAbsValue")
        if (Math.abs(value) > Math.abs(maxAbsValue)) maxAbsValue = value
        Log.e(TAG,"setValue maxAbsValue = $maxAbsValue")
    }

    fun clear() {
        Log.e(TAG,"clear")
//        if (data == null) return
        clearData()
        currValue = 0f
        maxAbsValue = currValue
        Log.e(TAG, "currValue = $currValue, maxAbsValue = $maxAbsValue")
    }

    override fun onDraw(canvas: Canvas) {
        val width = width
        val height = height

        if (width != this.width) return

        canvas.drawColor(backgroundColor)

        canvas.save()
        canvas.translate(0F, (height / 2 - 1).toFloat())
        canvas.scale(1F, 10F)
        run {
            // axis
            lp.color = axisColor
            canvas.drawLine(0F, 0F, (width - 1).toFloat(), 0F, lp)

            // border
            //lp.setColor(borderColor);
            //canvas.drawLine(0, 0, width - 1, 0, lp);
            //canvas.drawLine(width, 0, width - 1, height - 1, lp);
            //canvas.drawLine(width, height - 1, 0, height - 1, lp);
            //canvas.drawLine(0, height - 1, 0, 0, lp);

            // data
            if (data != null) {
                lp.color = dataColor
                canvas.translate((-framePos).toFloat(), 0F)
                canvas.drawLines(data, framePos * 4, (width - 1) * 4, lp)
            }
        }
        canvas.restore()

        // text
        lp.color = axisColor
        if (stringHeight == 0) {
            stringHeight = Math.round(Math.abs(lp.ascent()) + lp.descent() + 0.5f)
        }
        if (stringWidth == 0) {
            stringWidth = Math.round(lp.measureText("88.88") + 0.5f)
        }

        var str = String.format("%.1f", maxAbsValue)
        canvas.drawText(str, (width - 2 - stringWidth).toFloat(), (1 + stringHeight).toFloat(), lp)

        str = String.format("%.1f", currValue)
        canvas.drawText(str, (width - 2 - stringWidth).toFloat(), height - lp.descent(), lp)
    }

    private fun fillX() {
        Log.e(TAG, "fillX (data.size) / 4=${(data.size) / 4}")
//        if(data != null) {
        for (x in 0 until (data.size) / 4) {
            data[x * 4] = x.toFloat()
            Log.e(TAG, "data[x * 4] = ${data[x * 4]}")
            data[x * 4 + 2] = (x + 1).toFloat()
            Log.e(TAG, "data[x * 4 + 2]  = ${data[x * 4 + 2]}")
        }
//        }
    }

    companion object {
        private const val UPDATE_DELAY = 16 // 60 FPS
        private const val TEXT_SIZE = 10
        private const val TAG = "LineGraphView"
    }
}