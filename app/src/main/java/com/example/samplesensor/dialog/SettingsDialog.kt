package com.example.samplesensor.dialog

import android.widget.SeekBar
import android.widget.TextView
import android.os.Bundle
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.example.samplesensor.R
import com.example.samplesensor.settings.AppSettings






class SettingsDialog : DialogFragment() {

    private var settings: AppSettings? = null

    private lateinit var seekbarVelFriction: SeekBar
    private lateinit var txtVelFrictionSummary: TextView

    private lateinit var seekbarPosFriction: SeekBar
    private lateinit var txtPosFrictionSummary: TextView

    private lateinit var seekbarLowPassAlpha: SeekBar
    private lateinit var txtLowPassAlphaSummary: TextView

    private lateinit var seekbarVelocityAmpl: SeekBar
    private lateinit var txtVelocityAmplSummary: TextView

    private val sliderChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            settings?.let{
                when {
                    seekBar.id == R.id.seekbar_vel_friction -> {
                        if (fromUser) it.velocityFriction = progress / SCALE
                        txtVelFrictionSummary.text =
                            String.format("%.2f", it.velocityFriction)
                    }
                    seekBar.id == R.id.seekbar_pos_friction -> {
                        if (fromUser) it.positionFriction = progress / SCALE
                        txtPosFrictionSummary.text =
                            String.format("%.2f", it.positionFriction)
                    }
                    seekBar.id == R.id.seekbar_low_pass_alpha -> {
                        if (fromUser) it.lowPassAlpha = progress / SCALE
                        txtLowPassAlphaSummary.text = String.format("%.2f", it.lowPassAlpha)
                    }
                    seekBar.id == R.id.seekbar_velocity_ampl -> {
                        if (fromUser) it.velocityAmpl = progress + 5000
                        txtVelocityAmplSummary.text = String.format("%d", it.velocityAmpl)
                    }
                }
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {}

        override fun onStopTrackingTouch(seekBar: SeekBar) {}
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        settings = activity?.applicationContext?.let { AppSettings.getAppSettings(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.settings_dialog,container, false)
        seekbarVelFriction = view.findViewById(R.id.seekbar_vel_friction)
        txtVelFrictionSummary = view.findViewById(R.id.txt_vel_friction_summary)

        seekbarPosFriction = view.findViewById(R.id.seekbar_pos_friction)
        txtPosFrictionSummary = view.findViewById(R.id.txt_pos_friction_summary)

        seekbarLowPassAlpha = view.findViewById(R.id.seekbar_low_pass_alpha)
        txtLowPassAlphaSummary = view.findViewById(R.id.txt_low_pass_alpha_summary)

        seekbarVelocityAmpl = view.findViewById(R.id.seekbar_velocity_ampl)
        txtVelocityAmplSummary = view.findViewById(R.id.txt_velocity_ampl_summary)

        seekbarVelFriction.setOnSeekBarChangeListener(sliderChangeListener)
        seekbarVelFriction.max = SCALE.toInt()

        seekbarPosFriction.setOnSeekBarChangeListener(sliderChangeListener)
        seekbarPosFriction.max = SCALE.toInt()

        seekbarLowPassAlpha.setOnSeekBarChangeListener(sliderChangeListener)
        seekbarLowPassAlpha.max = SCALE.toInt()

        seekbarVelocityAmpl.setOnSeekBarChangeListener(sliderChangeListener)
        seekbarVelocityAmpl.max = 10000 // 5000 .. 15000
        fillViews()
        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = activity?.let {
            AlertDialog.Builder(it)
                .setPositiveButton(
                    android.R.string.ok
                ) { _, _ -> settings?.saveDeferred() }
        }

        return if (builder != null) {
            builder.setView(view)
            builder.create()
        } else {
            super.onCreateDialog(savedInstanceState)
        }
    }

    private fun fillViews() {
        settings?.let {
            seekbarVelFriction.progress = (it.velocityFriction * SCALE).toInt()
            seekbarPosFriction.progress = (it.positionFriction * SCALE).toInt()
            seekbarLowPassAlpha.progress = (it.lowPassAlpha * SCALE).toInt()
            seekbarVelocityAmpl.progress = it.velocityAmpl - 5000
        }
    }

    companion object {
        fun show(activity: FragmentActivity) {
            SettingsDialog().show(activity.supportFragmentManager,  "settings")
        }
        private const val SCALE = 1000f
    }


}