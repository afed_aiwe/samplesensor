package com.example.samplesensor

object Constants {
    val NS2S = 1.0f / 1000000000.0f
    val MAX_ACC = 5.0f
    val MAX_POS_SHIFT = 100.0f
    val MAX_ZOOM_FACTOR = 0.2f
}