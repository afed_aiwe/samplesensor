package com.example.samplesensor.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.os.Parcel
import android.app.Service
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.IBinder
import android.util.Log
import com.example.samplesensor.Constants
import com.example.samplesensor.settings.AppSettings
import com.example.samplesensor.utils.Utils
import java.lang.reflect.InvocationTargetException

class StabilizationService : Service() {

    private  var settings: AppSettings? = null
    private lateinit var sensorManager: SensorManager
    private var accelerometer: Sensor? = null

    private var accListenerRegistered = false

    private val tempAcc = FloatArray(3)
    private val acc = FloatArray(3)
    private val velocity = FloatArray(3)
    private val position = FloatArray(3)
    private var timestamp: Long = 0

    private var x = 0
    private var y = 0

    private var flinger: IBinder? = null

    private val sensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}

        override fun onSensorChanged(event: SensorEvent) {
            if (timestamp != 0L) {
                tempAcc[0] =
                    Utils.rangeValue(event.values[0], -Constants.MAX_ACC, Constants.MAX_ACC)
                tempAcc[1] =
                    Utils.rangeValue(event.values[1], -Constants.MAX_ACC, Constants.MAX_ACC)
                tempAcc[2] =
                    Utils.rangeValue(event.values[2], -Constants.MAX_ACC, Constants.MAX_ACC)

                settings?.let {
                    Utils.lowPassFilter(tempAcc, acc, it.lowPassAlpha)

                    val dt = (event.timestamp - timestamp) * Constants.NS2S

                    for (index in 0..2) {
                        velocity[index] += acc[index] * dt - it.velocityAmpl * velocity[index]
                        velocity[index] = Utils.fixNanOrInfinite(velocity[index])

                        position[index] += velocity[index] * it.velocityAmpl * dt - it.positionFriction * position[index]
                        position[index] = Utils.rangeValue(
                            position[index],
                            -Constants.MAX_POS_SHIFT,
                            Constants.MAX_POS_SHIFT
                        )
                    }
                }
            } else {
                velocity[2] = 0f
                velocity[1] = velocity[2]
                velocity[0] = velocity[1]
                position[2] = 0f
                position[1] = position[2]
                position[0] = position[1]

                acc[0] = Utils.rangeValue(event.values[0], -Constants.MAX_ACC, Constants.MAX_ACC)
                acc[1] = Utils.rangeValue(event.values[1], -Constants.MAX_ACC, Constants.MAX_ACC)
                acc[2] = Utils.rangeValue(event.values[2], -Constants.MAX_ACC, Constants.MAX_ACC)
            }

            timestamp = event.timestamp

            val newPosX = Math.round(position[0])
            val newPosY = Math.round(position[1])
            if (newPosX != x || newPosY != y) {
                x = newPosX
                y = newPosY
                setSurfaceFlingerTranslate(-x, y)
            }
        }
    }

    private val screenOnReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(TAG, "Screen is on")
            setSurfaceFlingerTranslate(0, 0)
            reset()
            registerAccListener()
        }
    }

    private val screenOffReceiver = object : BroadcastReceiver() {
       override fun onReceive(context: Context, intent: Intent) {
            unregisterAccListener()
            setSurfaceFlingerTranslate(0, 0)
            Log.d(TAG, "Screen is off")
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        settings = AppSettings.getAppSettings(applicationContext)

        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)

        registerReceiver(screenOnReceiver, IntentFilter(Intent.ACTION_SCREEN_ON))
        registerReceiver(screenOffReceiver, IntentFilter(Intent.ACTION_SCREEN_OFF))

        if (Utils.isScreenOn(this)) registerAccListener()
    }

    override fun onDestroy() {
        unregisterReceiver(screenOnReceiver)
        unregisterReceiver(screenOffReceiver)

        unregisterAccListener()
        setSurfaceFlingerTranslate(0, 0)

        super.onDestroy()
    }

    private fun reset() {
        position[2] = 0f
        position[1] = position[2]
        position[0] = position[1]
        velocity[2] = 0f
        velocity[1] = velocity[2]
        velocity[0] = velocity[1]
        acc[2] = 0f
        acc[1] = acc[2]
        acc[0] = acc[1]
        timestamp = 0
        y = 0
        x = y
    }

    private fun registerAccListener() {
        if (accListenerRegistered) return
        accListenerRegistered = sensorManager.registerListener(
            sensorEventListener,
            accelerometer,
            SensorManager.SENSOR_DELAY_FASTEST
        )
        if (!accListenerRegistered) {
            Log.wtf(TAG, "Sensor listener not registered")
        }
    }

    private fun unregisterAccListener() {
        if (accListenerRegistered) {
            accListenerRegistered = false
            sensorManager.unregisterListener(sensorEventListener)
        }
    }

    private fun setSurfaceFlingerTranslate(x: Int, y: Int) {
        try {
            if (flinger == null) flinger = getSurfaceFlingerTranslate("SurfaceFlinger")
            if (flinger == null) {
                Log.wtf(TAG, "SurfaceFlinger is null")
                return
            }

            val data = Parcel.obtain()
            data.writeInterfaceToken("android.ui.ISurfaceComposer")
            data.writeInt(x)
            data.writeInt(y)
            flinger?.let {
                it.transact(2020, data, null, 0)
            }
        } catch (e: Exception) {
            Log.e(TAG, "SurfaceFlinger error", e)
        }

    }

    private fun getSurfaceFlingerTranslate(serviceArg: String): IBinder? {
        var binder: IBinder? = null
        try {
            val method = Class.forName("android.os.ServiceManager").getMethod("getService", String::class.java)
            binder = method.invoke(null, serviceArg) as IBinder
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
        return binder
    }

    companion object {
        private val TAG = StabilizationService::class.java.simpleName
    }

}