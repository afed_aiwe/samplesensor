package com.example.samplesensor

import androidx.preference.PreferenceManager
import com.example.samplesensor.settings.AppSettings


class Application : android.app.Application() {
    var settings: AppSettings? = null
        private set

    override fun onCreate() {
        settings = AppSettings(PreferenceManager.getDefaultSharedPreferences(this))
        settings?.load()

        super.onCreate()
    }
}