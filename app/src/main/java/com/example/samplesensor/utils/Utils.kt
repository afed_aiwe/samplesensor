package com.example.samplesensor.utils

import android.content.Context
import android.view.Display
import android.hardware.display.DisplayManager


object Utils {
    fun isScreenOn(context: Context): Boolean {
        val dm = context.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
        for (display in dm.displays) {
            if (display.state != Display.STATE_OFF)
                return true
        }
        return false
    }

    fun lowPassFilter(input: FloatArray, output: FloatArray, alpha: Float) {
        for (i in input.indices)
            output[i] = output[i] + alpha * (input[i] - output[i])
    }

    fun rangeValue(value: Float, min: Float, max: Float): Float {
        if (value > max) return max
        return if (value < min) min else value
    }

    fun fixNanOrInfinite(value: Float): Float {
        return if (java.lang.Float.isNaN(value) || java.lang.Float.isInfinite(value)) 0f else value
    }
}