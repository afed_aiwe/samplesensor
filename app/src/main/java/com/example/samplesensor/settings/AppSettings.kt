package com.example.samplesensor.settings

import android.content.Context
import android.content.SharedPreferences
import com.example.samplesensor.Application


class AppSettings(private val prefs: SharedPreferences) : Settings() {

    fun load() {
        load(prefs)
    }

    fun save() {
        save(prefs)
    }

    fun saveDeferred() {
        saveDeferred(prefs)
    }

    companion object {
        fun getAppSettings(context: Context): AppSettings? {
            if (context is Application) return context.settings

            val appContext = context.applicationContext
            return if (appContext is Application) appContext.settings else null

        }
    }
}