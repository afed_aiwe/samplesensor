package com.example.samplesensor.settings

import android.content.SharedPreferences


open class Settings {

    var isServiceEnabled = SVC_ENABLED_DEFAULT
    var velocityFriction =
        VELOCITY_FRICTION_DEFAULT
    var positionFriction =
        POSITION_FRICTION_DEFAULT
    var lowPassAlpha = LOW_PASS_ALPHA_DEFAULT
    var velocityAmpl = VELOCITY_AMPL_DEFAULT

    fun load(prefs: SharedPreferences) {
        isServiceEnabled = prefs.getBoolean(
            SERVICE_ENABLED_KEY,
            SVC_ENABLED_DEFAULT
        )
        velocityFriction = prefs.getFloat(
            VELOCITY_FRICTION_KEY,
            VELOCITY_FRICTION_DEFAULT
        )
        positionFriction = prefs.getFloat(
            POSITION_FRICTION_KEY,
            POSITION_FRICTION_DEFAULT
        )
        lowPassAlpha = prefs.getFloat(
            LOW_PASS_ALPHA_KEY,
            LOW_PASS_ALPHA_DEFAULT
        )
        velocityAmpl = prefs.getInt(
            VELOCITY_AMPL_KEY,
            VELOCITY_AMPL_DEFAULT
        )
    }

    fun save(prefs: SharedPreferences) {
        val editor = prefs.edit()
        save(editor)
        editor.commit()
    }

    fun saveDeferred(prefs: SharedPreferences) {
        val editor = prefs.edit()
        save(editor)
        editor.apply()
    }

    fun save(editor: SharedPreferences.Editor) {
        editor.putBoolean(SERVICE_ENABLED_KEY, isServiceEnabled)
        editor.putFloat(VELOCITY_FRICTION_KEY, velocityFriction)
        editor.putFloat(POSITION_FRICTION_KEY, positionFriction)
        editor.putFloat(LOW_PASS_ALPHA_KEY, lowPassAlpha)
        editor.putInt(VELOCITY_AMPL_KEY, velocityAmpl)
    }

    companion object {
        private val SERVICE_ENABLED_KEY = "service_enabled"
        private val VELOCITY_FRICTION_KEY = "velocity_friction"
        private val POSITION_FRICTION_KEY = "position_friction"
        private val LOW_PASS_ALPHA_KEY = "low_pass_alpha"
        private val VELOCITY_AMPL_KEY = "velocity_ampl"

        private val SVC_ENABLED_DEFAULT = false
        private val VELOCITY_FRICTION_DEFAULT = 0.2f
        private val POSITION_FRICTION_DEFAULT = 0.1f
        private val LOW_PASS_ALPHA_DEFAULT = 0.85f
        private val VELOCITY_AMPL_DEFAULT = 10000
    }
}