package com.example.samplesensor.receivers

import com.example.samplesensor.service.StabilizationService
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import com.example.samplesensor.settings.AppSettings


class BootCompletedReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val settings = AppSettings.getAppSettings(context)
        if (settings != null) {
            if (settings.isServiceEnabled) {
                context.startService(Intent(context, StabilizationService::class.java))
            }
        }
    }
}